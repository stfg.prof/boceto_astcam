#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	camera = AstCam::getInstance();

	camera->setup();

	mouse = AstMouse::getInstance();

}

//--------------------------------------------------------------
void ofApp::update(){
	camera->update();
}

//desidir si centrar de -1 a 1 o como manejar eso
//--------------------------------------------------------------
void ofApp::draw(){
	ofBackgroundGradient(ofColor(50), ofColor(0));
	//camera->rollRad(ofGetElapsedTimeMillis());//ofGetMouseY()/ofGetHeight()*PI*2.f
											  //ofGetHeight()*PI*2.f

	camera->begin();

	//ofRect(-10.f,-10.f,20.f,20.f);
	//ofLine(0, 0, ofGetWidth(), ofGetHeight());
	ofPushMatrix();
	ofDrawGrid(20, 10, true, true, true, true);

//	ofRotateZ(ofGetMouseY());///ofGetHeight()*PI*2.f
	/*
	ofSetColor(255,0,175);
	ofLine(0, 0, ofGetWidth() / 4.f*cos(ofGetElapsedTimeMillis()*0.001f), ofGetHeight()/4.f);
	ofSetColor(255, 250, 0);
	ofLine(0, 0, ofGetWidth() / 4.f*sin(ofGetElapsedTimeMillis()*0.001f), ofGetHeight() / 4.f);
	*/

	//ofLine(ofGetWidth() / 4.f, ofGetWidth() / 4.f, ofGetWidth() / 2.f, ofGetHeight() / 2.f);
	ofSetColor(255, 250,255);
	ofLine(0,0, ofGetWidth() / 2.f, ofGetHeight() / 2.f);

	//ofLine(ofGetMouseX(), ofGetMouseY(), ofGetWidth(), ofGetHeight(), ofGetWidth(), ofGetHeight());
	ofPopMatrix();
	// Additional rendering ...

	// End rendering form the camera's perspective.
	camera->end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'w' || key == 'W')
	{
		camera->moveUp();

	}
	else if (key == 's' || key == 'S')
	{
		camera->moveDown();

	}
	else if (key == 'a' || key == 'A')
	{
		camera->moveLeft();

	}
	else if (key == 'd' || key == 'D')
	{
		camera->moveRight();

	}
	else if (key == 'q' || key == 'Q')
	{
		camera->zoomIn();

	}
	else if (key == 'e' || key == 'E')
	{
		camera->zoomOut();

	}
	else if (key == 'r' || key == 'R')
	{
		camera->rollClockwise();

	}
	else if (key == 'f' || key == 'F')
	{
		camera->rollCounterclockwise();

	}
	else if (key == 'z' || key == 'Z')
	{
		camera->setPos(ofVec3f(0, 0, 15));
		//camera->rollRad(.001f);

	}

	else if (key == 'c' || key == 'C')
	{
		camera->setPos(ofVec3f(0, 0, 10));

		//camera->rollRad(-.001f);

	}
	else if (key == 'm' || key == 'M')
	{
		camera->resetRoll();

	}
	else if (key == 'o' || key == 'O')
	{
		camera->setOrtho(true);

	}
	else if (key == 'p' || key == 'P')
	{
		camera->setOrtho(false);

	}
	else if (key == 'n' || key == 'N')
	{
		camera->resetPos();

	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
