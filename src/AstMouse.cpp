#include "AstMouse.h"


AstMouse* AstMouse::instance = nullptr;

AstMouse::AstMouse(){
	camera = AstCam::getInstance();
	bMouseOverride = false;
	enableListenerScroll();
	enableListenerDrag();
}
//----------------------------------------

AstMouse::~AstMouse() {
}
//----------------------------------------
AstMouse *AstMouse::getInstance()
{
	if (instance == nullptr) {
		instance = new AstMouse();
	}
	return instance;
}



void AstMouse::setMouseOverride(bool b) { bMouseOverride = b; }
bool AstMouse::getMouseOverride() { return bMouseOverride; }


void AstMouse::enableListenerScroll() {
	ofAddListener(ofEvents().mouseScrolled, this, &AstMouse::mouseScrolled, bMouseOverride ? OF_EVENT_ORDER_BEFORE_APP : OF_EVENT_ORDER_AFTER_APP);

}
void AstMouse::disableListenerScroll() {
	ofRemoveListener(ofEvents().mouseScrolled, this, &AstMouse::mouseScrolled, bMouseOverride ? OF_EVENT_ORDER_BEFORE_APP : OF_EVENT_ORDER_AFTER_APP);

}

void AstMouse::enableListenerDrag()
{
	ofAddListener(ofEvents().mouseDragged, this, &AstMouse::mouseDragged, bMouseOverride ? OF_EVENT_ORDER_BEFORE_APP : OF_EVENT_ORDER_AFTER_APP);
}
void AstMouse::disableListenerDrag()
{
	ofRemoveListener(ofEvents().mouseDragged, this, &AstMouse::mouseDragged, bMouseOverride ? OF_EVENT_ORDER_BEFORE_APP : OF_EVENT_ORDER_AFTER_APP);
}
///////////////////////////////////////////////////////////////////////////////////////////
///////////// metods
///////////////////////////////////////////////////////////////////////////////////////////
void AstMouse::mouseScrolled(ofMouseEventArgs & mouse) {
	camera->moveZoom(mouse.scrollY);
}
void AstMouse::mouseDragged(ofMouseEventArgs & mouse) {
	if (mouse.button == OF_MOUSE_BUTTON_MIDDLE)
	{
		camera->moveCam(ofVec2f(ofGetPreviousMouseX() - ofGetMouseX(), ofGetMouseY() - ofGetPreviousMouseY())*MOVE_SENSITIVITY);
	}
}
