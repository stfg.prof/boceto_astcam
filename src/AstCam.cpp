#include "AstCam.h"

////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////singleton intance//////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
AstCam* AstCam::instance = nullptr;

AstCam::AstCam()
{
	ortho = false;
}
//----------------------------------------

AstCam::~AstCam() {
}
//----------------------------------------
AstCam *AstCam::getInstance()
{
	if (instance == nullptr) {
		instance = new AstCam();
	}
	return instance;
}
//----------------------------------------



////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////central metods//////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
void AstCam::setup() {
	cam.setNearClip(0.01f);
	cam.setFarClip(1000.0f);

	//defining the real world coordinates of the window which is being headtracked is important for visual accuracy
	//windowWidth = ofGetWindowWidth();// 0.3f;
	//windowHeight = ofGetWindowHeight();//  0.2f;
	windowWidth = 1.f;
	windowHeight = 1.f;
	/*
	cout<<ofGetWindowWidth()<<endl;
	cout<<ofGetWindowHeight()<<endl;
	cout<<ofGetWindowSize()<<endl;
	*/

	//cout<<cam.screenToWorld(ofGetWindowSize())<<endl;
	//windowTopLeft = glm::vec3(-windowWidth / 2.0f,+windowHeight / 2.0f,0.0f);
	//windowBottomLeft = glm::vec3(-windowWidth / 2.0f,-windowHeight / 2.0f,0.0f);
	//windowBottomRight = glm::vec3(+windowWidth / 2.0f,-windowHeight / 2.0f,0.0f);
	viewerDistance = 20.f;
	//glm::vec3 pos(0, 0, viewerDistance);

	//headPosition = glm::vec3(0.5f * windowWidth * sin(ofGetElapsedTimef()), 0.5f * windowHeight * cos(ofGetElapsedTimef()), viewerDistance);
	//glm::vec3 pos = glm::vec3(0.5f * windowWidth, 0.5f * windowHeight, viewerDistance);
	pos=ofVec3f(0.5f * windowWidth, 0.5f * windowHeight, viewerDistance);
	cam.setPosition(pos);
	

}
void AstCam::update() {
	if (enabled) {
		//float tempDistance =ofGetMouseX();
		//glm::vec3 temp = glm::vec3(0.5f * windowWidth, 0.5f * windowHeight, tempDistance);
		cam.setPosition(pos);
		if (ortho) {
			cam.enableOrtho();
		}
		else {
			cam.disableOrtho();
		}
	}
}
void AstCam::begin() {
	if (enabled) {
		cam.begin();
	

	}
}
void AstCam::end() {
	if (enabled) {
		cam.end();
	}
}
void AstCam::setEnabled(bool b)
{
	enabled = b;
}

bool AstCam::getEnabled()
{
	return enabled;
}


////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////movement metods//////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

ofVec3f  AstCam::getPos() {	return ofVec3f(pos);}
float  AstCam::getScale() { return pos.z; }
float  AstCam::getDistance() { return pos.z; }
float  AstCam::getRot() { return cam.getRollRad(); }

void  AstCam::resetPos() {
	pos = ofVec3f(0.5f * windowWidth, 0.5f * windowHeight, viewerDistance);
}
void  AstCam::setPos(ofVec3f delta) {
	pos = delta;
}
void  AstCam::setPos(ofVec2f delta) {
	pos.x = delta.x;
	pos.y = delta.y;
}
void  AstCam::setPos(float x, float y) { 
	pos.x = x;
	pos.y = y;
}
void  AstCam::setScale(float delta) {
	pos.z = delta;
}
void  AstCam::setDistance(float delta) {
	pos.z = delta;
}
void AstCam::setOrtho(bool b)
{
	ortho = b;
}


void AstCam::rollRad(float delta)
{
	cam.rollRad(delta);
}
//esto genera siempre el giro o modifica el giro
void  AstCam::rollClockwise(float delta) {
	cam.rollRad(delta);
	}

void  AstCam::rollCounterclockwise(float delta) {
	cam.rollRad(-delta);
}
void AstCam::resetRoll()
{
	cam.rollRad(-cam.getRollRad());
}
void  AstCam::zoomIn(float delta ) { pos.z += delta; }
void  AstCam::zoomOut(float delta) { pos.z -= delta; }
void  AstCam::moveZoom(float delta) { pos.z += delta; }

//void  AstCam::moveAt(ofVec3f target) {}
//void  AstCam::moveTo(ofVec3f to) {}
//void  AstCam::moveBy(ofVec3f delta) {}

void  AstCam::moveCam(ofVec2f delta) {
	pos.x += delta.x; 
	pos.y += delta.y;
}

void  AstCam::moveLeft(float delta ) { pos.x -= delta; }
void  AstCam::moveRight(float delta ) { pos.x += delta; }
void  AstCam::moveUp(float delta ) { pos.y += delta; }
void  AstCam::moveDown(float delta ) { pos.y -= delta; }