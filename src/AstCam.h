#pragma once
#include "ofMain.h"
#define MOVE_DELTA 1.f
#define DEFAULT_ZOOM_DISTANCE 0
#define ZOOM_LIMIT 50
#define ZOOM_VELOCITY 2.0f
#define ROT_VELOCITY 0.0126
#define ZOOM_OUT_LIMIT 0.35
#define ZOOM_IN_LIMIT 60.0

class AstCam
{
public:
	static AstCam* getInstance();
	~AstCam();
	void setup();	
	void begin();
	void update();
	void end();
	void setEnabled(bool b);
	bool getEnabled();

	ofVec3f getPos();
	float getScale();
	float getDistance();
	float getRot();

	void resetPos( );

	void setOrtho(bool b);

	void setPos(float x, float y );
	void setPos(ofVec2f delta);
	void setPos(ofVec3f delta);
	void setScale(float delta);
	void setDistance(float delta);

	void rollRad(float delta);
	void resetRoll();
	void rollClockwise(float delta = ROT_VELOCITY );
	void rollCounterclockwise(float delta = ROT_VELOCITY );
	void zoomIn(float delta = ZOOM_VELOCITY);
	void zoomOut(float delta = ZOOM_VELOCITY);
	void moveZoom(float delta);

	void moveAt(ofVec3f target);
	void moveTo(ofVec3f to);
	void moveBy(ofVec3f delta); //renombrar moveCam
	void moveCam(ofVec2f delta);
	void moveLeft(float delta = MOVE_DELTA);
	void moveRight(float delta = MOVE_DELTA);
	void moveUp(float delta = MOVE_DELTA);
	void moveDown(float delta = MOVE_DELTA);

protected:
	ofCamera cam;
	ofVec3f pos; //realmente podria usar los atributos propios de la camara heredados de ofNode
	//ofVec3f pos; // generar una rotacion?
	bool ortho;
	//float zoom;
	glm::vec3 windowTopLeft;
	glm::vec3 windowBottomLeft;
	glm::vec3 windowBottomRight;
	float windowWidth;
	float windowHeight;
	float viewerDistance;


private:
	static AstCam* instance;
	AstCam();
	bool enabled = true;

};

