#pragma once
#include "ofMain.h"
#include "AstCam.h"
#define MOVE_SENSITIVITY .25f

class AstMouse
{
public:
	static AstMouse* getInstance();

	~AstMouse();
	void mouseScrolled(ofMouseEventArgs & mouse);
	void mouseDragged(ofMouseEventArgs & mouse);
	
	void setMouseOverride(bool b);
	bool getMouseOverride();
protected:
	AstCam * camera;
	//que onda este viewport??
	//virtual void begin(ofRectangle viewport = ofGetCurrentViewport());

	//publico?
	//los enable y disable listener tendria que ser una func tipo dispatch ? haciendo callbacks
	void enableListenerDrag();
	void disableListenerDrag();
	void enableListenerScroll();
	void disableListenerScroll();

	bool bMouseOverride;

private:
	static AstMouse* instance;
	AstMouse();
};

