# camara para remplazar el canva infinito que se utiliza de camara en [AudioStellar](https://gitlab.com/ayrsd/audiostellar)

## To Do:
- [x] camara
- [x] testeo con teclas
- [x] Clase Mouse
- [x] Event-listener en Mouse
- [ ] revisar como se componen los units (un circle + un circle invisible)
- [ ] readaptar y sincronizar las dimensiones de los units con la visualizacion de los mismos
- [ ] migrar a AudioStellar
